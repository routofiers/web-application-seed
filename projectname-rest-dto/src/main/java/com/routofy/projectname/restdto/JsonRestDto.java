package com.routofy.projectname.restdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by prateek on 8/21/2015.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JsonRestDto {
    @JsonProperty("f")
    public int first;

    @JsonProperty("s")
    public int second;
}
