import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by naman on 8/1/16.
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JsonDto {
    public int first;
    public int second;

    public JsonDto createDoubleOfSelf() {
        return new JsonDto(first * 2, second * 2);
    }
}
