package com.routofy.projectname.health;

import com.codahale.metrics.health.HealthCheck;

/**
 * Created by prateek on 8/21/2015.
 */
public class ProjectnameHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        //todo : put meaningful sanity checks
        return Result.healthy();
    }
}
