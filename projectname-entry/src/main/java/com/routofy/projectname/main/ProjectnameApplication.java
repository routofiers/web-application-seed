package com.routofy.projectname.main;

import com.routofy.projectname.health.ProjectnameHealthCheck;
import com.routofy.projectname.rest.ProjectnameResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Created by peace on 21/8/15.
 */
public class ProjectnameApplication extends Application<ProjectnameConfig>{
    @Override
    public void run(ProjectnameConfig configuration, Environment environment) throws Exception {
        final ProjectnameResource resource = new ProjectnameResource();
        final ProjectnameHealthCheck healthCheck = new ProjectnameHealthCheck();
        environment.healthChecks().register("projectname-health-check", healthCheck);
        environment.jersey().register(resource);
    }

    public static void main(String[] args) throws Exception {
        new ProjectnameApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ProjectnameConfig> bootstrap) {
    }

    @Override
    public String getName() {
        return "projectname-app";
    }
}
