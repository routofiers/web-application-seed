package com.routofy.projectname.rest;

import com.routofy.projectname.restdto.JsonRestDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by prateek on 8/21/2015.
 */
@Path("v1")
public class ProjectnameResource {
    @Path("ping")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String ping() {
        return "pong";
    }

    @Path("ping-json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public JsonRestDto getLoc() {
        return new JsonRestDto(12, 23);
    }

    @Path("ping-json")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public JsonRestDto postLoc(JsonRestDto jsonRestDto) {
        return new JsonRestDto();
    }
}
